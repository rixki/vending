class Vending {
  food = [
    {
      id: 1,
      name: 'Biscuit',
      value: 6000,
      stock: 5,
    },
    {
      id: 2,
      name: 'Chips',
      value: 8000,
      stock: 5,
    },
    {
      id: 3,
      name: 'Oreo',
      value: 10000,
      stock: 5,
    },
    {
      id: 4,
      name: 'Tango',
      value: 12000,
      stock: 5,
    },
    {
      id: 5,
      name: 'Cokelat',
      value: 15000,
      stock: 5,
    },
  ];
  selectedFood = {};
  acceptedNominal = [
    2000, 5000, 10000, 20000, 50000,
  ];
  deposits = 0;

  display = () => {
    console.log(`Our menu:`)
    this.food.map(f => {
      if (f.stock > 0) console.log(`${f.id} | ${f.name} - Rp${f.value}`)
    })
  }
  choose = async id => {
    // check if stock > 0
    const choosing = await this.food.map(f => {
      if (f.id === id) {
        switch(f.stock) {
          case 0:
            console.log(`Sorry selected product not available`);
            break;
          default:
            console.log(`choose => ${f.name}, processing`)
            if (this.selectedFood[f.id]) {
              this.selectedFood[f.id].count += 1
            } else {
              this.selectedFood[f.id] = { id: f.id, name: f.name, count: 1, value: f.value }
            }
            // subtract the stock here
            f.stock -= 1
            break;
        }
        console.log(this.selectedFood);
        this.recalculate();
        return f;
      } else {
        return f;
      }
    })

    this.food = choosing
  }
  recalculate = (final = false, notify = true) => {
    const selected = this.selectedFood
    let subTotal = 0;
    for (const [_key, value] of Object.entries(selected)) {
      subTotal += (value.value * value.count)
    }
    if (notify) console.log(`${final?`final`:`current`} amount to pay: ${subTotal}`)
    return subTotal;
  }
  deposit = amount => {
    if (this.acceptedNominal.includes(amount)) {
      this.deposits += amount;
      console.log(`current deposit: ${this.deposits}`)
      return;
    }

    console.log(`cannot accept the nominal, returning the money: ${amount}`);
    return;
  }
  acceptMoney = money => {
    let state = money
    let temp = {
      '50000': 0
    };

    const modBy50 = money % 50000

    if (modBy50 >= 0) {
      const prev = money - modBy50;
      temp['50000'] = prev / 50000;
      state -= prev
    }

    const modBy20 = state % 20000

    if (modBy20 >= 0) {
      const prev = state - modBy20;
      temp['20000'] = prev / 20000;
      state -= prev
    }

    const modBy10 = state % 10000

    if (modBy10 >= 0) {
      const prev = state - modBy10;
      temp['10000'] = prev / 10000;
      state -= prev
    }

    const modBy5 = state % 5000

    if (modBy5 >= 0) {
      const prev = state - modBy5;
      temp['5000'] = prev / 5000;
      state -= prev
    }

    const modBy2 = state % 2000

    if (modBy2 >= 0) {
      const prev = state - modBy2;
      temp['2000'] = prev / 2000;
      state -= prev
    }

    const units = {};
    for (const [key, value] of Object.entries(temp)) {
      if (value) units[key] = value;
    }

    return {
      balance: state,
      units
    }
  }
  pay = () => {
    if (this.recalculate(true, false) === 0) {
      console.log(`you are not pay for nothing`);
      return;
    }
    const needToPay = this.recalculate(true, false)
    if (this.deposits < needToPay) {
      const deficit = needToPay - this.deposits
      console.log(`transaction failed, please insert more cash, deficit: ${deficit}`)
      return;
    }

    const changes = this.deposits - needToPay
    if (changes >= 0) {
      // deplete the product
      console.log(`thank you for using this machine`)
      console.log(`please wait, depleting the food`)
      const selected = this.selectedFood
      for (const [_key, value] of Object.entries(selected)) {
        console.log(`send to tray: ${value.name} x${value.count}`)
      }
      // withdraw money here
      this.abortTransaction(changes, false, true)
      console.log(`transaction complete`);
      return;
    }
  }
  cancel = () => {
    // revert back from selected to food
    console.log(`cancelling the order`)
    const temp = this.food.map(f => {
      for (const [_key, value] of Object.entries(this.selectedFood)) {
        if (f.id === value.id) f.stock += value.count;
      }
      return f;
    })

    this.food = temp;
    this.abortTransaction(this.deposits, true, true)
  }
  abortTransaction = (changes = 0, notify = true, withdraw = false) => {
    const getMoneyFromChanges = this.acceptMoney(changes ?? this.deposits)
    if (notify) {
      console.log(`aborting transaction`)
    }
    if (withdraw) {
      console.log(`withdraw your changes:`, getMoneyFromChanges.units)
      console.log(`withdraw your changes with coins:`, getMoneyFromChanges.balance)
    }
    this.deposits = 0
    this.selectedFood = {}
  }
}

module.exports = Vending