const Vending = require("./lib/vending");

const vending = new Vending

// display menu with active stock
vending.display();

// choose product
vending.choose(1);
vending.choose(1);
vending.choose(3);
vending.choose(4);

// calculate final price
vending.recalculate(true);

// try to deposit money
vending.deposit(20000); // machine accept
vending.deposit(100000); // machine not accept
vending.deposit(50000); // machine accept

// cancel
vending.cancel(); // <-- if this enable, the pay below cannot be processed

// pay
vending.pay(); // <-- only if cancel above is disabled

vending.recalculate(true);

// after paid or cancelled
console.log(vending.food); // <-- if want to know the stock changes